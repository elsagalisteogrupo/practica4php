<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

        <form method="get">
            <div>
                <label for="a">A</label>
                <input type="number" id="a" name="a">
            </div>
            <div>
                <label for="b">B</label>
                <input type="number" id="b" name="b">
            </div>
            <div>
                <label for="c">C</label>
                <input type="number" id="c" name="c">
            </div>
            <div>
                <label for="a">A</label>
                <button name="enviar" type="submit">Calcular</button>
            </div>
        </form>

        <?php
        if (isset($_GET["enviar"])) {
            $a = $_GET["a"];
            $b = $_GET["b"];
            $c = $_GET["c"];

            $p = ($a + $b + $c) / 2;
            $area = sqrt($p * ($p - $a) * ($p - $b) * ($p - $c));
            echo "El resultado es " . $area;
        }
        ?>
    </body>
</html>
